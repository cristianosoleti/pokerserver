import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { PokerTableModule } from './poker-table/poker-table.module';
import { GameServerModule } from './game-server/game-server.module';

@Module({
  imports: [GameServerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
