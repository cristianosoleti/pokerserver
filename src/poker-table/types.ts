export type Suit = 'CLUB' | 'DIAMOND' | 'HEART' | 'SPADE';
export interface Card {
  suit: Suit;
  value: number;
}
