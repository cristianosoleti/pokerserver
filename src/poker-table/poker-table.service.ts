import { Injectable, Logger } from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { Card } from './types';
import { shuffle } from 'lodash';

@Injectable()
@WebSocketGateway(3001)
export class PokerTableService
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  @WebSocketServer()
  server: Server;
  private logger: Logger = new Logger('PokerTableService');
  private deck: Card[] = [];
  private isGameStarted = false;
  private isTurn = false;
  private isRiver = false;
  private numberOfPlayers = 0;
  private gameRounds = 0;
  private cardsInRound: Card[] = [];
  private maxSeats = 5;

  afterInit() {
    this.logger.log('PokerTable Init -- Waiting for players');
  }

  setDeck(deck: Card[]): void {
    this.deck = deck;
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
    this.numberOfPlayers--;
  }

  handleConnection(client: Socket, ...args: any[]) {
    this.logger.log(`Client connected: ${client.id}${client.handshake.query}`);
    if (this.numberOfPlayers === this.maxSeats) {
      client.disconnect();
      return;
    }
    this.numberOfPlayers++;
  }

  shuffleDeck() {
    this.deck = shuffle(this.deck);
  }

  startGame() {
    this.isGameStarted = true;
    this.shuffleDeck();
    this.cardsInRound = [];
  }

  stopGame() {
    this.isGameStarted = false;
    this.isTurn = false;
    this.isRiver = false;
  }

  @SubscribeMessage('player-participating')
  async identity(
    @MessageBody() data: number,
    @ConnectedSocket() client: Socket,
  ): Promise<any> {
    let cardsToDeal: Card[];
    if (this.isTurn) {
      this.isRiver = true;
      cardsToDeal = [this.deck[4]];
      this.stopGame();
      // process winnings
      // and restart
    } else if (this.isGameStarted) {
      this.isTurn = true;
      cardsToDeal = [this.deck[3]];
    } else {
      this.startGame();
      cardsToDeal = this.deck.slice(0, 3);
    }
    this.cardsInRound = this.cardsInRound.concat(cardsToDeal);
    this.server.emit('poker', {
      cards: cardsToDeal,
      start: this.cardsInRound.length === 3,
    });
    return { data: 'test' };
  }
}
