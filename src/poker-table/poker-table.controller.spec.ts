import { Test, TestingModule } from '@nestjs/testing';
import { PokerTableController } from './poker-table.controller';

describe('PokerTableController', () => {
  let controller: PokerTableController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PokerTableController],
    }).compile();

    controller = module.get<PokerTableController>(PokerTableController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
