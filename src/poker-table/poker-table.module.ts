import { Module } from '@nestjs/common';
import { PokerTableService } from './poker-table.service';
import { PokerTableController } from './poker-table.controller';

@Module({
  providers: [PokerTableService],
  controllers: [PokerTableController],
})
export class PokerTableModule {}
