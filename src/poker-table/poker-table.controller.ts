import { Controller, Get } from '@nestjs/common';
import { shuffle } from 'lodash';
import { PokerTableService } from './poker-table.service';
import { Card, Suit } from './types';

const suits: [Suit, Suit, Suit, Suit] = ['CLUB', 'DIAMOND', 'HEART', 'SPADE'];
const values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
const deck: Card[] = (() => {
  let lDeck: Card[] = [];
  for (const suit of suits) {
    for (const value of values) {
      lDeck.push({
        suit,
        value,
      });
    }
  }
  lDeck = shuffle(lDeck);
  return lDeck;
})();

@Controller('poker-table')
export class PokerTableController {
  constructor(private readonly pokerService: PokerTableService) {
    pokerService.setDeck(deck);
  }

  @Get()
  getHello(): string {
    return '123';
  }
}
