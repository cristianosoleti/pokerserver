import { Test, TestingModule } from '@nestjs/testing';
import { PokerTableService } from './poker-table.service';

describe('PokerTableService', () => {
  let service: PokerTableService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PokerTableService],
    }).compile();

    service = module.get<PokerTableService>(PokerTableService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
