import { Module } from '@nestjs/common';
import { GameServerController } from './game-server.controller';
import { GameServerService } from './game-server.service';

@Module({
  providers: [GameServerService],
  controllers: [GameServerController],
})
export class GameServerModule {}
