import { Injectable, Logger } from '@nestjs/common';
import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@Injectable()
@WebSocketGateway(3002)
export class GameServerService
  implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
  @WebSocketServer()
  server: Server;
  private logger: Logger = new Logger('GameServerService');
  private numberOfPlayers = 0;
  private maxConnections = 10000;

  getNumberOfPlayers() {
    return this.numberOfPlayers;
  }

  afterInit() {
    this.logger.log('Game server init -- Waiting for players');
  }

  handleDisconnect(client: Socket) {
    this.logger.log(`Client disconnected: ${client.id}`);
    this.numberOfPlayers--;
  }

  handleConnection(client: Socket) {
    const serverId = client.handshake.query.serverId;
    this.logger.log(
      `Client connected: ${client.id} - requesting game server: ${serverId}`,
    );

    // Validate server ID

    // Check if connections are too many
    if (this.numberOfPlayers === this.maxConnections) {
      client.disconnect();
      return;
    }
    this.numberOfPlayers++;

    const serverRoom = this.getServerRoomName(serverId);
    client.join(serverRoom);

    this.logger.log(`Client connected to - ${serverRoom}`);

    this.server.emit(serverRoom, {
      message: `WELCOME_TO_${serverRoom}`,
      value: '34',
    });
  }

  // message should not be any but strongly typed
  emitToServer(serverId: number, message: any) {
    const serverRoom = this.getServerRoomName(serverId);
    this.server.emit(serverRoom, {
      message,
      value: Math.random(),
    });
  }

  private getServerRoomName(serverId: number) {
    return `SERVER_${serverId}`;
  }
}
