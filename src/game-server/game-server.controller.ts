import { Body, Controller, Get, Logger, Post } from '@nestjs/common';
import { GameServerService } from './game-server.service';

type MessageDispatch = {
  serverId: number;
  message: string;
  value: string;
};

@Controller('game-server')
export class GameServerController {
  constructor(private readonly gameServerServive: GameServerService) {}
  private logger: Logger = new Logger('GameServerController');

  @Get()
  getPlayersConnected(): number {
    return this.gameServerServive.getNumberOfPlayers();
  }

  @Post()
  create(@Body() body: MessageDispatch) {
    this.logger.log(body);
    this.gameServerServive.emitToServer(body.serverId, body.message);
    return {
      success: true,
    };
  }
}
